from . import models
from django import forms

class JadwalForm(forms.ModelForm):
        name = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Nama Kegiatan",
                }))
        desc = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Deskripsi Kegiatan",
                }))
        category = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Kategori",
                }))
        location = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Tempat",
                }))
        date = forms.DateField(widget=forms.SelectDateWidget(attrs={
                "class" : "datefield jadwalfields",
                "required" : True,
                }))
        time = forms.TimeField(widget=forms.TimeInput(attrs={
                "class" : "datefield jadwalfields",
                "placeholder":"00:00",
                "required" : True,
                }))
        
        class Meta:
                model = models.Jadwal
                fields = ["name", "desc", "category", "location", "date", "time"]
                error_messages = {
                        'name': {
                                'validators': ("Name is too short."),
                        },
                        'name': {
                                'max_length': ("Name is too long."),
                        },
                        'desc': {
                                'validators': ("Description is too short."),
                        },
                        'desc': {
                                'max_length': ("Description is too long."),
                        },
                        'category': {
                                'max_length': ("Category is too long."),
                        },
                        'location': {
                                'max_length': ("Location is too long."),
                        },
                }