from django.db import models
from django.utils import timezone

# Create your models here.
class Jadwal(models.Model):
    name = models.CharField(null=False, max_length=50)
    desc = models.CharField(null=True, max_length=100)
    category = models.CharField(null=True, max_length=30)
    location = models.CharField(null=True, max_length=30)
    date = models.DateField(default=timezone.now)
    time = models.TimeField(default=timezone.now)

    def __str__(self):
        return self.name

