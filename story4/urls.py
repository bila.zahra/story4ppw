from django.urls import path
from django.contrib import admin
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('', views.resume, name='resume'),
    path('create', views.create_jadwal),
    path('', views.jadwal, name='jadwal'),
    path('delete', views.delete)
]